# Angular大合集

## 推荐的项目

##### Angular+NgRx实现Step表单，并加入了常见的表单逻辑，例如表单联动，表单验证，步骤2跳转到步骤4等等。
https://gitee.com/leeyamaster/Angular-Step-Forms

##### Angular+NgRx全家桶，包含Store, Entity, Component-Store, Router-Store, Component-View, Effects + MockJs模拟请求。（推荐） 
https://gitee.com/leeyamaster/angular-ngrx-demo

##### Angular集成NgRx Store ，Entity，Router Store 打造TodoList
https://gitee.com/leeyamaster/Angular-TodoList

##### Angular+Egg全栈实现RBAC权限管理
- https://gitee.com/leeyamaster/angular-rbac
- https://gitee.com/leeyamaster/rights-management-of-egg-rbac

##### Angular Zorro后台管理系统，还没开发完，等遇到私活，再继续开发。rxjs处理异步是真的很强大。
- https://gitee.com/leeyamaster/angular-admin

## 以下是Demo

##### Angular17新特性Signals的小Demo
https://gitee.com/leeyamaster/angular-signals

##### 使用NgRx-Data + angular-in-memory-web-api开发TodoList，NgRx Data资料很少，码云基本没有，Github上比较多，不过这个框架很笨重，一旦需求复杂点，那么这框架改起来特别麻烦，并且文档、资料少，不推荐使用这个框架。
https://gitee.com/leeyamaster/angular-ng-rx-data

##### 通过angular formly表单处理框架来处理步骤条表单，formly框架文章资源特别少，并且扩展性很差，不推荐使用，跟alibaba formily差远了。
https://gitee.com/leeyamaster/angular-formly-step

##### Angular+实现Jira的拖动，列表的拖动，盒子的拖动。（部分未完成）
https://gitee.com/leeyamaster/angular-drag

##### 包含Rxjs的练习，Rxjs实现表格拖动表头改变宽度，拖动两个盒子间细线改变盒子宽度。（部分未完成）
https://gitee.com/leeyamaster/angular-rx-js-demo

## Angular文档与资料
- https://angular.cn/docs
- https://ng.ant.design/docs/introduce/zh
- https://ngrx.io/guide/data